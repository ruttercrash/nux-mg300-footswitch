#include "MIDIUSB.h"

const byte DEVICE_INQUIRY[] =  {
  0xF0, 0x00, 0x11, 0x22, 0x66, 0x00, 0x00, 0x00, 0x00, 0xF7
};
const byte STATUS_REQUEST[] = {
  0xF0, 0x00, 0x11, 0x22, 0x66, 0x00, 0x70, 0x19, 0x00, 0xF7
};
const int BUTTON = 11;

int lastState = HIGH;
int currentState;

void changePatch(uint8_t patchNumber) {
  midiEventPacket_t midiCC = {0x0B, 0xB0, 60, patchNumber};
  MidiUSB.sendMIDI(midiCC);
  MidiUSB.flush();
  /*  
  Serial.write(176); // MIDI continous controller command
  Serial.write(60); // nux mg-300 patch change controller
  Serial.write(patchNumber); // patch number 0-71
  */
}

void setup() {
  pinMode(BUTTON, INPUT_PULLUP);
  
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  currentState = digitalRead(BUTTON);
  
  if (lastState != currentState) { 
    digitalWrite(LED_BUILTIN, HIGH);
    /*
    Serial.write(DEVICE_INQUIRY, sizeof(DEVICE_INQUIRY));
    Serial.write(STATUS_REQUEST, sizeof(STATUS_REQUEST));
    */
    if (currentState == LOW) {
      changePatch(1);
    } else {
      changePatch(0);
    }
    lastState = currentState;
    delay(25);
    digitalWrite(LED_BUILTIN, LOW);

  }
}
